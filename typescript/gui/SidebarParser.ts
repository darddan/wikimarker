const wiki_hybris = new DetectParsePair(
	() => {
		return document.getElementsByClassName("rw_pagetree")[0] != undefined;
	},
	() => {
		return [].slice.call([].slice.call(document.getElementsByClassName("rw_pagetree"))[0].getElementsByTagName("a"));
	}
);

const help_hybris_commerce = new DetectParsePair(
	() => {
		return document.getElementById("nav-content") != null;
	},
	() => {
		return [].slice.call(document.getElementById("nav-content").getElementsByTagName("a")).filter(x => x.offsetParent !== null);
	}
);

const detectParseList: DetectParsePair[] = [wiki_hybris, help_hybris_commerce];

class SidebarParser {
	static getLinks(): HTMLAnchorElement[] {
		for (const i of detectParseList) {
			if (i.valid()) {
				return i.list();
			}
		}
		return null;
	}
}
