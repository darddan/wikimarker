class Gui {
	private fab: Fab;

	constructor() {
		this.fab = new Fab();
	}

	async render(): Promise<void> {
		Log.debug("Gui rendering started.")
		await Service.init();

		Service.onStorageChanged(() => {
			this.resetValues();
		});

		// Wait until the sidebar is loaded
		while (SidebarParser.getLinks() == null)
			await Util.sleep(100);
		
		this.fab.show();
		this.resetValues();

		Log.debug("Gui is rendered.")
	}

	private async resetValues(): Promise<void> {
		const isMarked = await Service.isCurrentMarked();
		// browser.browserAction.setBadgeText(isMarked? "Selected":"Not Selected");
		this.fab.changeValue(isMarked);
		this.colorSidebar();
	}

	private setLinkStyle(link: HTMLAnchorElement, marked: boolean): void {
		if (marked)
    		link.style.background = "#333", link.style.color = "#EEE";
		else 
    		link.style.background = "#EEE", link.style.color = "#333";
	}

	private async colorSidebar(): Promise<void> {
		const links: HTMLAnchorElement[] = SidebarParser.getLinks();

		var debug_msg: String = "Should be colored?\n";
		for (const link of links) {
			const href = link.href.split('#')[0];
			const marked = await Service.isMarked(href);
			debug_msg += `${marked} - ${href}\n`;
			this.setLinkStyle(link, marked);
		}
		Log.info(debug_msg);
	}
}
