class Fab {
	private element: HTMLAnchorElement;

	private content: string;
	private background: string;
	private color: string;
	private hoverBG: string;

	private setMarked(): void {
		this.content = "Mark as unread";
		this.background = "red";
		this.color = "white";
		this.hoverBG = "#ff5656";
	}

	private setUnmarked(): void {
		this.content = "Mark as read";
		this.background = "white";
		this.color = "red";
		this.hoverBG = "#ffebe8";
	}

	private setValues(): void {
		this.element.textContent = this.content;
		this.element.style.background = this.background;
		this.element.style.color = this.color;
	}

	private setHover(): void {
		var hoverBG = this.hoverBG;
		var background = this.background;
		this.element.addEventListener("mouseover", function() {
			this.style.background = hoverBG;
		});
		this.element.addEventListener("mouseout", function() {
			this.style.background = background;
		});
	}

	show(): void {
		document.body.appendChild(this.element);
	}

	changeValue(marked: boolean): void {
		if (marked) this.setMarked();
		else this.setUnmarked();
		this.setValues();
		this.setHover();
	}

	constructor() {
		this.element = document.createElement("a");

		this.element.style.position = "fixed";
		this.element.style.top = "150px";
		this.element.style.right = "20px";

		this.element.style["border-radius"] = "6px";
		this.element.style["text-decoration"] = "none";
		this.element.style.border = "1px solid red";
		this.element.style.padding = "3px 12px";
		this.element.href = "";

		this.element.addEventListener("click", function(event) {
			event.preventDefault();
			Service.toggleCurrent();
		});
	}
}