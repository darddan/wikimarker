declare var browser;

class Persistence {
	private static cache: SortedList;

	static async loadCache(): Promise<void> {
		let db = await browser.storage.local.get();
		
		if (Util.isEmpty(db)) {
			Persistence.cache = SortedList.emptyList();
			Persistence.saveCache();
		} else {
			Persistence.cache = SortedList.fromSortedArray(db.list);
		}

	}

	private static async resolveCache(): Promise<void> {
		let db = await browser.storage.local.get();
		Persistence.cache = SortedList.fromSortedArray(db.list);
	}

	private static async saveCache(): Promise<void> {
		let db = {"list": Persistence.cache.getArray()}
		await browser.storage.local.set(db);
	}

	static async getList(): Promise<SortedList> {
		await Persistence.loadCache();
		return Persistence.cache;
	}

	static async setList(value: SortedList): Promise<void> {
		Persistence.cache = value;
		Persistence.saveCache();
	}

	static onStorageChanged(handler: () => void): void {
		browser.storage.onChanged.addListener(function(changes, areaname) {
			if (areaname == "local") {
				handler();
			}
		});
	}
}