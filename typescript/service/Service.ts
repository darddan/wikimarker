class Service {
	static async init(): Promise<void> {
		await Persistence.loadCache();
	}

	static async isCurrentMarked(): Promise<boolean> {
		return Service.isMarked(link);
	}

	static async isMarked(link: String): Promise<boolean> {
		const list = await Persistence.getList();
		return list.contains(link);
	}

	static async toggleCurrent(): Promise<void> {
		const marked: boolean = await Service.isCurrentMarked();

		var list = await Persistence.getList();

		Log.debug(list.dump());
		if (marked)
			list.remove(link);
		else
			list.add(link);

		Log.debug(list.dump());
		await Persistence.setList(list);
	}

	static async getList(): Promise<SortedList> {
		return await Persistence.getList();
	}

	static onStorageChanged(handler: () => void): void {
		Persistence.onStorageChanged(handler);
	}
}