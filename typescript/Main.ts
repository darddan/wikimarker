Log.setLevel(LogLevel.NONE);

Log.always("The application has started. \nRunning the gui.");

var gui: Gui = new Gui();
gui.render();
