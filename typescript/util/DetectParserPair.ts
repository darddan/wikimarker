class DetectParsePair {
	private validFn: () => boolean;
	private parseFn: () => HTMLAnchorElement[];
	constructor(validFn: () => boolean, parseFn: () => HTMLAnchorElement[]) {
		this.validFn = validFn;
		this.parseFn = parseFn;
	}

	valid(): boolean {
		return this.validFn();
	}

	list(): HTMLAnchorElement[] {
		return this.parseFn();
	}
}
