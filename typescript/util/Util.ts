let link = window.location.href.split('#')[0];

class Util {
	static isEmpty(cache): boolean {
		return cache != null &&
			Object.keys(cache).length === 0 &&
			cache.constructor === Object;
	}
	
	static sleep(ms: number): Promise<void> {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}
}
