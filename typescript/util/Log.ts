enum LogLevel {
	INFO = 1,
	DEBUG = 2,
	ERROR = 3,
	NONE = 4,
}

class Log {
	private static lvl: LogLevel = LogLevel.DEBUG;
	private static app: String = "WikiMarker";

	static setLevel(lvl: LogLevel): void {
		Log.lvl = lvl;
	}

	private static printLog(type: String, color: String, message: String): void {
		const app = Log.app;
		const time = Log.getTime();

		console.log(`%c${app} ${type} %c${time}:`, Log.getCss(color, true), Log.getCss(color, false) );
		
		const lines = message.split('\n');
		for (const line of lines)
			console.log("\t" + line);
	}

	static info(message: String): void {
		if (Log.lvl <= LogLevel.INFO)
			Log.printLog("INFO", "lightseagreen", message);
	}

	static debug(message: String): void {
		if (Log.lvl <= LogLevel.DEBUG)
			Log.printLog("DEBUG", "orange", message);
	}

	static error(message: String): void {
		if (Log.lvl <= LogLevel.ERROR)
			Log.printLog("ERROR", "red", message);
	}

	static always(message: String): void {
		Log.printLog("-", "royalblue", message);
	}

	private static getCss(color: String, big: boolean): String {
		const size = big ? 1.2 : 1;
		const weight = big ? "bold" : "normal";
		return `color: ${color}; font-weight: ${weight}; font-size: ${size}em;`;
	}

	private static getTime(): String {
		return new Date().toLocaleString();
	}
}