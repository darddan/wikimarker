class SortedList {
	private list: String[];

	private constructor(list: String[]) {
		this.list = list;
	}

	private sort(): void {
		this.list = this.list.sort((one, two) => (one > two ? -1 : 1));
	}

	static emptyList(): SortedList {
		return new SortedList([]);
	}

	static fromArray(list: String[]): SortedList {
		var ret = new SortedList(list);
		ret.sort();
		return ret;
	}

	static fromSortedArray(list: String[]): SortedList {
		return new SortedList(list);
	}

	nextPos(value: String): number {
		var begin: number = 0, end: number = this.list.length;

		while (begin < end) {
			const center: number = (begin + end) / 2;
			if (this.list[center] < value)
				begin = center + 1;
			else
				end = center;
		}

		return begin;
	}

	contains(value: String): boolean {
		// TODO: Implement binary search
		return this.list.includes(value);
	}

	add(value: String): boolean {
		if (this.contains(value))
			return false;
		
		this.list.splice(this.nextPos(value), 0, value);
		return true;
	}

	remove(value: String): boolean {
		if (!this.contains(value))
			return false;


		const pos = this.list.indexOf(value);

		this.list.splice(pos, 1);
		return true;
	}

	getArray(): String[] {
		return this.list;
	}

	dump(): String {
		var ret = "List: \n";
		for (var link of this.list) {
			ret = ret + "\t" + link + "\n";
		}
		return ret;
	}

}

